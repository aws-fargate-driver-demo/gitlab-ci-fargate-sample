# Node.js sample for GitLab CI on AWS Fargate

Sample Node.js repository demonstrating how to run GitLab CI pipelines with the
[AWS Fargate Custom Executor driver](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate)
for [GitLab Runner](https://docs.gitlab.com/runner).

[![pipeline
status](https://gitlab.com/aws-fargate-driver-demo/gitlab-ci-fargate-sample/badges/master/pipeline.svg)](https://gitlab.com/aws-fargate-driver-demo/gitlab-ci-fargate-sample/-/commits/master)

## Instructions

### 1. Prepare a Docker image to run the CI pipelines

The image must ship with all tools required to run a given CI pipeline and
accept SSH connections through Public-key Authentication.

By _all tools_ we mean the programs used by the CI jobs, e.g. `java (JDK)`,
`mvn` and/or `gradle` if your team codes in Java; `node`, `npm` and/or `yarn`
if Node.js; `python` and `pip` if Python; and so on. Also, `gitlab-runner`
binary must be in the container `$PATH` [if there are artifacts to be uploaded
or cache to be managed](https://docs.gitlab.com/runner/executors/custom.html#run)
when processing any of the jobs.

You'll notice the CI Job Image is not specified in the `.gitlab-ci.yml` file,
but in the _Task Definition_ (step 4).

As a reference, the present sample can use an image built from [this
repository](https://gitlab.com/aws-fargate-driver-demo/docker-nodejs-gitlab-ci-fargate).

First of all, build the image:

```sh
docker build -t <repository-name> .
```

### 2. Push the image to a container registry

#### 2.1 Amazon ECR

1. Retrieve an authentication token and authenticate Docker client to your
   registry. Use the AWS CLI:

   ```sh
   aws ecr get-login-password --region <region> \
     | docker login --username AWS --password-stdin <account-id>.dkr.ecr.<region>.amazonaws.com/<repository-name>
   ```

1. Tag your image so you can push it to ECR:

   ```sh
   docker tag <repository-name>:latest <account-id>.dkr.ecr.<region>.amazonaws.com/<repository-name>:latest
   ```

1. Push to the ECR repository:

   ```sh
   docker push <account-id>.dkr.ecr.<region>.amazonaws.com/<repository-name>:latest
   ```

#### 2.2 GitLab Container Registry

**TO BE DONE!**

### 3. Store the SSH public key in AWS Parameter Store

In AWS Console, go to _Management & Governance/Systems Manager_, then
_Application Management/Parameter Store_. Create a parameter of type `String`.
There’s no need to use `SecureString` since it will store a public key.

Set the parameter value to the public key that will allow SSH connections to the
containers responsible for running CI Jobs.

### 4. Create an ECS Task Definition

Minimal configuration:

```json
{
  "executionRoleArn": "arn:aws:iam::<account-id>:role/ecsTaskExecutionRole",
  "containerDefinitions": [
    {
      "portMappings": [
        {
          "hostPort": 22,
          "protocol": "tcp",
          "containerPort": 22
        }
      ],
      "secrets": [
        {
          "valueFrom": "arn:aws:ssm:<region>:<account-id>:parameter/<parameter-name>",
          "name": "SSH_PUBLIC_KEY"
        }
      ],
      "image": "<account-id>.dkr.ecr.<region>.amazonaws.com/<repository-name>:latest",
      "name": "fargate-gitlab-ci-container"
    }
  ],
  "memory": "1024",
  "family": "fargate-gitlab-ci",
  "requiresCompatibilities": ["FARGATE"],
  "networkMode": "awsvpc",
  "cpu": "512"
}
```

Please notice it refers to the ECR image uploaded in step 2.1
(`containerDefinitions.image`) and the parameter stored in step 3
(`containerDefinitions.secrets.valueFrom`), hence the role specified in
`executionRoleArn` must have read access to both.

The `containerDefinitions.secrets` object is responsible for injecting the SSH
public key in each container through the `SSH_PUBLIC_KEY` environment variable.

### 5. Create an ECS Fargate cluster

Nothing special here... Just follow the [standard
instructions](https://docs.aws.amazon.com/AmazonECS/latest/userguide/create_cluster.html).

### 6. Set up a GitLab Runner

Provision an EC2 instance -- `t2.micro` is the minimum size recommended for
hosting the runner. In this case study, the EC2 instance is not intended to run
any jobs, it will only host **GitLab Runner** and the **Fargate driver**, which
will trigger the CI jobs on ECS. Either _Ubuntu_, _Debian_, _CentOS_ or _RHEL_
distributions are recommended.

[Install GitLab Runner using the official GitLab
repositories](https://docs.gitlab.com/runner/install/linux-repository.html).

[Register](https://docs.gitlab.com/runner/register/index.html) a GitLab Runner.
When asked for the _executor type_, type `custom`.

### 7. Set up the Fargate driver

Please refer to the **AWS Fargate driver for Custom Executor**
[documentation](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate/-/tree/master/docs)
for details.
